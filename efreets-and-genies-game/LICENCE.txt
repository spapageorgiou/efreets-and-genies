License. 
I, Solon P. Papageorgiou from Cyprus, Nicosia,
wrote this Web application in 2014.
I am the sole owner of the application's code and nobody else is allowed to use that code in any way.
However, that code will also belong to the next
company where I will work and they can do anything they want with it. 
I will not own in any way any code that my next company,
may write using the code of this application.

This license overrides any other license information
that may be found in other application's code files.