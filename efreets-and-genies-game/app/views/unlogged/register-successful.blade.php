@extends('layout')

@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <p>
            Registration has been successful.
        </p>
        <p>Click <a href="/">here</a> to log in.</p>
    </div>
</div>    
@stop



