In Efreets And Genies, you choose an Efreet or a Genie as race and move around 
killing monsters and increasing your skills. 
Type commands + enter in the text field or click "Commands" button, to see
the list of your available commands. 
There are 3 types of monsters that appear according to your level. You have to
find them on the map. 
The highest you can get in skills and level is excellent. You don't die in this 
game, but your score is penalized if your health is low.
If you reach score of 100, you win the game, but you can continue playing.

