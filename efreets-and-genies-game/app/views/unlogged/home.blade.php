@extends('layout')

@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4 text-center">
        <p class="bold">Welcome To Efreets And Genies, a mud like browser game!</p>
    </div>
</div>
<form action="login-post-redirect-get" method="POST" class="form-horizontal">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Login</h3>
                </div>
                <div  class="panel-body">
                    <p>                    
                        Please complete all fields to login.
                    </p>                    
                    @if(Session::has('message'))
                        <p><span class="center-block label label-danger">{{ Session::get('message') }}</span></p> 
                    @endif                    
                    @if($errors->has('username')) 
                        <p><span class="center-block label label-danger">{{ $errors->first('username') }}</span></p> 
                    @endif                    
                    <div class="form-group">
                        <label for="username" class="col-lg-4 control-label">Username:</label>
                        <div class="col-lg-7">
                            <input name="username" id="username" type="text" class="form-control" value="{{{ Input::old('username') }}}" placeholder="Michael">                             
                        </div>
                    </div>                    
                    @if($errors->has('password')) 
                        <p><span class="center-block label label-danger">{{ $errors->first('password') }}</span></p> 
                    @endif                    
                    <div class="form-group">
                        <label for="password" class="col-lg-4 control-label">Password:</label> 
                        <div class="col-lg-7">
                            <input name="password" id="password" type="password" class="form-control" value="{{{ Input::old('password') }}}" placeholder="Ab123#">
                        </div>
                    </div>                
                    <p>
                        <input type="submit" value="Login" class="btn btn-success pull-right">
                    </p>                   
                </div>
            </div>
            <p>
                Demo Account: username: <span class="bold">demouser</span> password: <span class="bold">demouser</span> .
            </p>
            <p>
                @include('unlogged/include/help-text')
            </p>         
        </div>    
    </div>
</form>
@stop



