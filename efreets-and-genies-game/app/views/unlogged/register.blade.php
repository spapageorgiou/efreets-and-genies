@extends('layout')

@section('content')
<form action="register-post-redirect-get" method="POST" class="form-horizontal">
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    <div class="row">
        <div class="col-md-5 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Registration</h3>
                </div>
                <div  class="panel-body">
                    <p>                    
                        Please complete all fields to register.
                    </p>                    
                    @if($errors->has('username')) 
                        <p><span class="center-block label label-danger">{{ $errors->first('username') }}</span></p> 
                    @endif                                  
                    <div class="form-group">
                        <label for="username" class="col-lg-4 control-label">Username:</label>
                        <div class="col-lg-7">
                            <input name="username" id="username" type="text" class="form-control" value="{{{ Input::old('username') }}}" placeholder="Michael">                             
                        </div>
                    </div>                    
                    @if($errors->has('password')) 
                        <p><span class="center-block label label-danger">{{ $errors->first('password') }}</span></p>
                    @endif                    
                    <div class="form-group">
                        <label for="password" class="col-lg-4 control-label">Password:</label> 
                        <div class="col-lg-7">
                            <input name="password" id="password" type="password" class="form-control" value="{{{ Input::old('password') }}}" placeholder="Ab123#">
                        </div>
                    </div>                    
                    @if($errors->has('password-retyped')) 
                        <p><span class="center-block label label-danger">{{ $errors->first('password-retyped') }}</span></p>
                    @endif                        
                    <div class="form-group">
                        <label for="password-retyped" class="col-lg-4 control-label">Retype Password:</label>       
                        <div class="col-lg-7">
                            <input name="password-retyped" id="password-retyped" type="password" class="form-control" value="{{{ Input::old('password-retyped') }}}" placeholder="Ab123#">
                        </div>
                    </div>                    
                    <?php
                        $captcha = new General\Laravel4x\Various\Validation\Validation;
                    ?>
                    @if($errors->has('captcha')) 
                        <p><span class="center-block label label-danger">{{ $errors->first('captcha') }}</span></p>
                    @endif                        
                    <div class="form-group">
                        <label for="captcha" class="col-lg-4 control-label">Type Text(Captcha):</label>       
                        <div class="col-lg-7">
                            <div class="input-group input-group-lg">
                                <span class="input-group-addon"><img src="{{ $captcha->makeCaptcha('captcha') }}" height="22" width="90" alt="Verification Text"></span>                
                                <input name="captcha" id="captcha" type="text" class="form-control" value="{{{ Input::old('captcha') }}}" placeholder="type text">
                            </div>
                        </div>
                    </div>   
                    <p>
                        <input type="submit" value="Register" class="btn btn-success pull-right">
                    </p>
                </div>
            </div>
        </div>    
    </div>
</form>
@stop

