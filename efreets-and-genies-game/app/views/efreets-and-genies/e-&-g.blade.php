@extends('layout')


@include('logged/include/logged-menu')

@section('content')
<script src="js/js-custom/general/feature-sniffing.js"></script>
<script src="js/js-custom/general/general.js"></script>
<script src="js/js-custom/efreets-genies/game-runner.js?1"></script>
<script src="js/js-custom/efreets-genies/commands.js?1"></script>
<script src="js/js-custom/efreets-genies/messages.js"></script>
<script src="js/js-custom/efreets-genies/ajax.js"></script>
<script src="js/js-custom/efreets-genies/character.js?3"></script>
<script src="js/js-custom/efreets-genies/maps.js"></script>
<script src="js/js-custom/efreets-genies/opponent.js"></script>
<script src="js/js-custom/efreets-genies/fight.js?1"></script>
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
{{-- Save user id in JavaScript. With it you will save highscore of user. --}}
@if(Session::has('userId'))
<script>
        var user = { id:{{ Session::get('userId') }} };
</script>
@endif
<noscript>Your browser does not support JavaScript or JavaScript is disabled! This game requires JavaScript to run.</noscript>
<div class="row">
    <div class="col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Main Window</h3>
            </div>
            <div class="panel-body" id="game-window-main">
                <div id="game-intro-message" class="hide">
                    <p>
                        @if(Session::has('username'))
                        <span class='bold orange'>{{{ Session::get('username') }}}</span>
                        @endif
                        welcome to Efreets and Genies!
                    </p>
                    <p>Click <a href="help">here</a> for help, or if you are ready click below button to play.</p>
                    <div class="btn-group">
                        <p><input type="submit" id="start-game" value="Start Game" class="btn btn-default pull-left"></p>
                    </div>
                </div>
                <div id="no-features" class="hide">
                    Canvas or JSON are not supported in your browser. The game cannot run.
                </div>
                <div id="race-message" class="hide">
                    <p class="bold">Choose a race:</p>
                </div>
                <div id="choose-race" class="hide">                    
                    <div class="btn-group">
                        <input type="submit" id="choose-efreet" value="Efreet" class="btn btn-default pull-left">                
                        <input type="submit" id="choose-genie" value="Genie"  class="btn btn-default pull-left">
                    </div>                    
                </div>
                <div id="main-window" class="hide">
                    <p><label for="commands-field">Enter Command:</label></p>                    
                    <div class="row">
                        <div class="col-md-10">
                            <div class="input-group">
                                <input name="commands-field" placeholder="Command + Enter." id="commands-field" type="text" list="autocomplete-commands" class="form-control">
                                <span class="input-group-btn">
                                    <button type="button" id="submit-command" class="btn btn-success pull-left">Command</button>
                                </span>
                            </div>   
                        </div> 
                    </div>                    
                    <datalist id="autocomplete-commands"></datalist>
                    <div id="map">
                        <p>
                            <canvas id="game-canvas" width="200" height="200"></canvas>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <div class="col-md-4 hide" id="game-panel-command-buttons">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Command Buttons</h3>
            </div>
            <div  class="panel-body">
                <div id="game-window-buttons">
                </div>
            </div>
        </div>
    </div>   
    <div class="col-md-4 hide" id="game-panel-messages">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Messages</h3>
            </div>
            <div  class="panel-body" id="game-window-messages">
                <p>
                    <textarea rows="16" cols="38" readonly id="messages">
                    </textarea>
                </p>
            </div>
        </div>
    </div>
</div>
@stop