<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title>Efreets And Genies</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="description" content="Free Mud Like Game">
        <meta name="keywords" content="browser,based,game,mud,short">
        <meta name="author" content="Solonas Papageorgiou">
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico">
        <!-- Bootstrap third party css. -->
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <!-- DataTables third party css -->
        <link rel="stylesheet" href="js/vendor/DataTables-1.10.4/DataTables-1.10.4/media/css/jquery.dataTables.css">
        <link rel="stylesheet" href="js/vendor/DataTables-1.10.4/DataTables-1.10.4/extensions/Responsive/css/dataTables.responsive.css">
        <!-- Custom css. -->
        <link rel="stylesheet" href="css/css-custom/main.css?2">
        <!-- Scripts -->
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        <script src="js/vendor/jquery-1.11.0.js"></script>
        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/vendor/underscore-min.js"></script>
    </head>
    <body>
        <!--[if lt IE 9]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->                  
        <?php
        $urls = array();
        $urls['/'] = "Home";
        $urls['register'] = "Register";    
        ?>
        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">                        
                        @section('menu-buttons')
                            @for($i = 0; $i < count($urls); $i++)
                                <span class="icon-bar"></span>
                            @endfor
                        @show
                    </button>
                    <a class="navbar-brand" href="/"><img src="img/efreets-and-genies-logo.png" alt="Efreets and Genies"/>&nbsp;Efreets And Genies</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        @section('menu-links')                        
                            @foreach($urls as $link => $text)
                                <li <?= Request::is($link) ? "class=\"active\"" : '' ?>><a href="{{$link}}">{{$text}}</a></li>                         
                            @endforeach 
                        @show                       
                    </ul>
                </div><!--/.nav-collapse -->
            </div>
        </nav>
        <main class="container">
            @yield('content')
        </main> <!-- /container -->    
        <div class="container">
            <div class="row">    
                <footer class="col-md-4 col-md-offset-4">
                    <hr>
                    <p>&copy; 2014 by Solonas Papageorgiou. All rights reserved.</p>
                </footer>
            </div>
        </div>        
        {{-- Google Analytics: change UA-XXXXX-X to be your site's ID. --}}
        <?php
        $urlsKeys = array_keys($urls);

        $googleAnalyticsUrls = array();
        $googleAnalyticsUrls[] = $urlsKeys[0];
        $googleAnalyticsUrls[] = $urlsKeys[1];        
        ?>
        @foreach($googleAnalyticsUrls as $url)                
            @if(Request::is($url))            
                @include('unlogged/include/google-analytics/google-analytics')
            @endif
        @endforeach        
    </body>
</html>
