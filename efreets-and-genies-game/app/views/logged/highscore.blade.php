@extends('layout')

@include('logged/include/logged-menu')

@section('content')
<!-- DataTables -->
<script src="js/vendor/DataTables-1.10.4/DataTables-1.10.4/media/js/jquery.dataTables.js"></script>
<script src="js/vendor/DataTables-1.10.4/DataTables-1.10.4/extensions/Responsive/js/dataTables.responsive.js"></script>
<script>
$(document).ready( function () {
    $('#table-scores-id').DataTable({
        responsive: true
    });
} );
</script>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        @if(isset($result))
         <table id="table-scores-id" class="table-scores">
             <thead>
                <tr>
                    <th>Username</th>
                    <th>Character Name</th>                    
                    <th>Race</th>
                    <th>Score</th>
                    <th>Date</th>
                </tr>  
             </thead>
             <tbody>
                @foreach($result as $column)
                    <tr>
                        <td>{{{ $column->username }}}</td>
                        <td>{{{ $column->name }}}</td>                        
                        <td>{{{ $column->race }}}</td>
                        <td>{{{ $column->score }}}</td>
                        <td>{{ $column->updated_at }}</td>
                    </tr>
                @endforeach 
            </tbody>
         </table>        
        @else
            <p>
                No high scores available.
            </p>
        @endif     
    </div>
</div>    
@stop



