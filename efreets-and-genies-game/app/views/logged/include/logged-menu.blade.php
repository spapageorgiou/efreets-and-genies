@section('menu-buttons')
    @include('logged/include/logged-menu-buttons', $urls)
@overwrite

@section('menu-links')
    @include('logged/include/logged-menu-links', $urls)
@overwrite