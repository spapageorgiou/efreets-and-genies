@extends('layout')

@include('logged/include/logged-menu')

@section('content')
<div class="row">
    <div class="col-md-4 col-md-offset-4">
        <p>
            @include('unlogged/include/help-text')
        </p>
    </div>
</div>    
@stop

