<?php

/* 
* Register custom validations.
 */

(new General\Laravel4X\Various\Validation\Validation)->notExists();

(new General\Laravel4X\Various\Validation\Validation)->validateCaptcha();