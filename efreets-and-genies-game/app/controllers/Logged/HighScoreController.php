<?php

namespace Logged;

/*
 * HighScore Controller. It shows high scores.
 */
class HighScoreController extends \BaseController {

    private $urls;
    
    private $db;
    
    public function __construct(\GeneralProject\Laravel4X\Various\URLS\URL $urls,
            HighScore\DB $db) {
        
        $this->urls = $urls;
        
        $this->db = $db;
        
    }
    
    /*
     * Show 10 highest scores.
     */
    public function showHighScores(){
        
        $urls = $this->urls->getLoggedURLs();
        
        $result = $this->db->showHighScores();
        
        if($result == true){
            
            \Log::info("HighScores have been retrieved successfully.");
            
            return \View::make('logged/highscore', array('urls' => $urls, 'result' => $result));
        }
        else if($result === false){
            
            \Log::info("No HighScores to retrieve.");
            
            return \View::make('logged/highscore', array('urls' => $urls));
            
        }        
    }
}
