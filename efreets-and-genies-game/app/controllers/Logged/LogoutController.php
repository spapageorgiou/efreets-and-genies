<?php

namespace Logged;

/*
 * LogoutController is responsible for logging out the user.
 */
class LogoutController extends \BaseController {
    
    /*
     * Process logout.
     */
    public function process(){
        
        //Clears existing sessions.
        \Session::flush(); 
        
        \Log::info('User has been logged out.');
        
        return \Redirect::route('logout-successful');
    }
}
