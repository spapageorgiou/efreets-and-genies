<?php

namespace EfreetsAndGenies;

/*
 * High score controller.
 */

class HighScoreAJAXController extends \BaseController {
    
    private $highscore;
    
    private $jsonResponse; 
    

    public function __construct(HighScoreAJAX\DB $highscore,
                                \General\Laravel4X\Controller\Response\Response $response) {
                                                          
        $this->highscore = $highscore;
        
        $this->jsonResponse = $response->returnJSON();
    }

    /**
     * Save char.
     */
    public function save() {
      
        $input = \Input::all();

        $inputUserId = $input["userId"];
        
        $inputName = $input["name"];

        $inputScore = $input["score"];

        $inputRaceId = $input["raceId"];

        $queryParameters = array();

        //For insert.
        $queryParameters[] = $inputUserId;
        
        $queryParameters[] = $inputName;

        $queryParameters[] = $inputScore;

        $queryParameters[] = $inputRaceId;
        
        $queryParameters[] = date('Y-m-d H:i:s');
        
        //For update.
        $queryParameters[] = $inputName;

        $queryParameters[] = $inputScore;

        $queryParameters[] = $inputRaceId;
        
        $highscore = $this->highscore;

        $result = $highscore->insertHighScore($queryParameters);

        $response = $this->jsonResponse;
        
        if($result === true) {
            
            \Log::info('Succeeded saving Efreet And Genies character score.');   
            
            return $response;
            
        }
        
    }

    /*
     * Keep session unlive, to be able to save score.
     */
    
    public function updateSession() {

        return $this->jsonResponse;
    }

}
