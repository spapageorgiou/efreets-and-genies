<?php

namespace Unlogged;

/*
 * Login Controller.
 */

class LoginController extends \BaseController {

    private $validator;
    private $db;

    public function __construct(Login\Validator $validator,            
            Login\DB $db) {

        $this->validator = $validator;

        $this->db = $db;
    }

    /*
     * Process page.
     */

    public function process() {

        $input = \Input::all();

        $username = $input["username"];

        $password = $input["password"];

        //Validate.        
        $validationResult = $this->validator->validate($username, $password);

        if ($validationResult === false) {
            return \Redirect::route('home')->withInput()->withErrors($this->validator->getValidator());
        }
        // Validation succeeded.
        else {

            return $this->startLogin($username, $password);
        }
    }

    /*
     *  Do login related things, then login.
     */

    private function startLogin($username, $password) {

        return $this->processPassword($username, $password);
    }

    /*
     * Get salt for user, rehash and login.
     */

    private function processPassword($username, $password) {
        
        $queryParameters = array();

        $queryParameters[] = $username;

        $result = $this->db->getSalt($queryParameters);

        if ($result === false) {
            return \Redirect::route('home')->withInput()->withMessage('It seems you did not register. Please do.');
        } else if ($result == true) {
            
            // Salt found for user. Rehash.
            
            $passwordHash = $this->validator->rehashPassword($password, $result[0]->salt);
            
            return $this->login($username, $passwordHash);
        }
    }

    /*
     * Actual Login.
     */

    private function login($username, $passwordHash) {

        $queryParameters = array();

        $queryParameters[] = $username;

        $queryParameters[] = $passwordHash;

        $result = $this->db->attemptLogin($queryParameters);

        if ($result === false) {
            return \Redirect::route('home')->withInput()->withMessage('Wrong Credentials. Please retry.');
        } else if ($result === true) {
            return \Redirect::route('e-&-g');
        }
    }

}
