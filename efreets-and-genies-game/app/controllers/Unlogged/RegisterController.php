<?php

namespace Unlogged;

/*
 * Register Controller. 
 */

class RegisterController extends \BaseController {

    private $validator;
    private $db;

    public function __construct(Register\Validator $validator,            
                                Register\DB $db) {

        $this->validator = $validator;

        $this->db = $db;

    }

    /*
     * Process page.
     */

    public function process() {

        $input = \Input::all();

        $username = $input["username"];

        $password = $input["password"];

        $passwordRetyped = $input["password-retyped"];

        $captcha = $input["captcha"];

        //Validate.        
        $validationResult = $this->validator->validate($username, $password, $passwordRetyped, $captcha);

        if ($validationResult === false) {
            return \Redirect::route('register')->withInput()->withErrors($this->validator->getValidator());
        }
        // Validation succeeded.
        else {

            return $this->startRegister($username, $password);
        }
    }

    /*
     * Do things related to registration, then register.
     */

    private function startRegister($username, $password) {
        
        list($passwordHash, $randomSalt) = $this->validator->buildPassword($password);

        return $this->register($username, $passwordHash, $randomSalt);
    }

    /*
     * Actual register.
     */

    private function register($username, $passwordHash, $randomSalt) {

        $queryParameters = array();

        $queryParameters[] = $username;

        $queryParameters[] = $passwordHash;

        $queryParameters[] = $randomSalt;

        $result = $this->db->register($queryParameters);

        if ($result === true) {

            \Log::info('Succeeded registering user.');

            return \Redirect::route('register-successful');
        }
    }

}
