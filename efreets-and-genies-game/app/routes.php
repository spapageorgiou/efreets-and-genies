<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

$urls = (new GeneralProject\Laravel4X\Various\URLS\URL)->getLoggedURLs();

Route::group(array('before' => array('unlogged_pages')), function() {
    Route::get('/', array('as' => 'home', function() {
            return View::make('unlogged/home');
        }));

    Route::get('register', array('as' => 'register', function() {
            return View::make('unlogged/register');
        }));
});

Route::group(array('before' => array('csrf')), function() {
    Route::post('login-post-redirect-get', array('uses' => 'Unlogged\LoginController@process'));

    Route::post('register-post-redirect-get', array('uses' => 'Unlogged\RegisterController@process'));
});

Route::get('register-successful', array('as' => 'register-successful', function() {
        return View::make('unlogged/register-successful');
    }));

Route::group(array('before' => array('logged_pages')), function() use($urls) {

    Route::get('e-and-g', array('as' => 'e-&-g', function() use($urls) {
            return View::make('efreets-and-genies/e-&-g', array('urls' => $urls));
    }));

    Route::get('highscore', array('uses' => 'Logged\HighScoreController@showHighScores'));

    Route::get('help', function() use($urls) {
           return View::make('logged/help', array('urls' => $urls));
    });

    Route::get('logout', array('uses' => 'Logged\LogoutController@process'));
    
    Route::group(array('prefix' => 'ajax-pages', 'before' => array('generic_ajax', 'csrf')), function() {

          Route::post('e-and-g-save-highscore', array('before' => array('e_and_g_ajax', 'user_id_check_ajax'),
                     'uses' => 'EfreetsAndGenies\HighScoreAJAXController@save'));

          Route::post('e-and-g-update', array('uses' => 'EfreetsAndGenies\HighScoreAJAXController@updateSession'));
    });
});


Route::get('logout-successful', array('as' => 'logout-successful', function() {
        return View::make('unlogged/logout-successful');
    }));        