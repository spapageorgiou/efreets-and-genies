<?php

namespace Unlogged\Register;

/*
 * DB code for Register.
 */

class DB {

    public function register($queryParameters) {
        $query = "INSERT "
                . "INTO users "
                . "(username, password, salt) "
                . "VALUES "
                . "(?, ?, ?) "
                . ";";

        try {

            $result = \DB::insert($query, $queryParameters); 

            return $result;
            
        } catch (Exception $ex) {
            
            \Log::error($ex->getMessage());
            \App::abort(500);
        }
    }

}
