<?php

namespace Unlogged\Register;

/*
 * Validation
 */

class Validator {

    private $validator;
    private $password;
    private $testerBlowfishPw;
    private $stringHelper;

    public function __construct(\General\Utilities\Fundamentals\Security\Password $password,
            \General\Laravel4X\Security\Validation\Validation $testerBlowfishPw,
            \General\Utilities\Fundamentals\Helpers\StringHelper $stringHelper) {

        $this->password = $password;
        
        $this->testerBlowfishPw = $testerBlowfishPw;

        $this->stringHelper = $stringHelper;
    }

    /*
     * Validate.
     */

    public function validate($username, $password, $passwordRetyped, $captcha) {

        $messages = array(
            'not_exists' => 'The :attribute exists. Choose another.',
            'validate_captcha' => 'Captcha text does not match image. Retry.'
        );

        $this->validator = \Validator::make(
                        array(
                    'username' => $username,
                    'password' => $password,
                    'password-retyped' => $passwordRetyped,
                    'captcha' => $captcha
                        ), array(
                    'username' => 'required|min:6|not_exists:users,username',
                    'password' => 'required|min:6',
                    'password-retyped' => 'required|min:6|same:password',
                    'captcha' => 'required|validate_captcha:captcha'
                        ), $messages
        );

        if ($this->validator->fails()) {

            return false;
        } else {

            return true;
        }
    }

    public function getValidator() {

        return $this->validator;
    }

    /*
     * Build password.
     */

    public function buildPassword($password) {

        //BlowFish uses a salt of 22 alphanumeric characters.
        $length = 22;

        $cost = 10;

        $randomSalt = $this->stringHelper->getRandomAlphaNum($length);

        $passwordHash = $this->password->cryptBlowfish($password, $cost, $randomSalt);

        //Error with hashing.
        $this->testerBlowfishPw->testBlowfishPw($passwordHash);

        return array($passwordHash, $randomSalt);
    }

}
