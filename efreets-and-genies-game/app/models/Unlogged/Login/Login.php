<?php

namespace Unlogged\Login;

/*
 * Login related stuff.
 *
 */

class Login {
    /*
     * Create login sessions.
     */

    public function setSessions($result) {        
        
        //Save the user id in a Session. Setting this Session means that user logged in.
        \Session::put('userId', $result[0]->user_id);

        //Save username in Session for later display.
        \Session::put('username', $result[0]->username);
    }

}
