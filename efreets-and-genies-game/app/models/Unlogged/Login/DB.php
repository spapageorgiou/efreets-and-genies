<?php

namespace Unlogged\Login;

/*
 * Description of DB for Login.
 */

class DB {
    
    private $login;
    
    public function __construct(Login $login)
    {
        $this->login = $login;
    }
    
    /*
     * Get salt.
     */
    
    public function getSalt($queryParameters) {

        $query = "
        SELECT         
        salt
        FROM
        users
        WHERE
        username = ?
        ;
        ";

        try {
            $result = \DB::select($query, $queryParameters);

            if(count($result) === 1){
                
                \Log::info('Salt found for user: ' . $queryParameters[0]);
                
                return $result;
            }
            else if(count($result) === 0){
                
                \Log::info('Salt not found for user: ' . $queryParameters[0]);
                
                return false;
            }            
            
        } catch (Exception $ex) {
            \Log::error($ex->getMessage());
            \App::abort(500);
        }
    }    
    
    /*
     * Attempt login.
     */

    public function attemptLogin($queryParameters) {
        $query = "SELECT "
                . "COUNT(username) as `count`, "
                . "id as `user_id`, "
                . "username "
                . "FROM users "
                . "WHERE "
                . "username = ? "
                . "AND "
                . "password = ? "
                . ";";

        try {

            $result = \DB::select($query, $queryParameters);

            if (count($result) === 1) {

                \Log::info('Attempting to log user.');

                $resultCount = (int)$result[0]->count;                

                if ($resultCount === 0) {

                    \Log::info('Failed to log user. Wrong Credentials');
                    
                    return false;
                    
                } else if ($resultCount === 1) {

                    \Log::info('Succeeded logging user.');
                    \Log::info('Username: ' . $result[0]->username);

                    $this->login->setSessions($result);                    
                    
                    return true;
                }
            }
        } catch (Exception $ex) {

            \Log::error($ex->getMessage());
            \App::abort(500);
        }
    }

}
