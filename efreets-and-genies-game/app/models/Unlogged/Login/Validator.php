<?php

namespace Unlogged\Login;

/**
 * Validator for Login.
 */
class Validator {

    private $validator;
    private $password;
    private $testerBlowfishPw;

    public function __construct(\General\Utilities\Fundamentals\Security\Password $password,
                                \General\Laravel4X\Security\Validation\Validation $testerBlowfishPw) {
        
                $this->password = $password;
                
                $this->testerBlowfishPw = $testerBlowfishPw;
        
    }
    
    /*
     * Validate.
     */

    public function validate($username, $password) {


        $this->validator = \Validator::make(
                        array(
                    'username' => $username,
                    'password' => $password
                        ), array(
                    'username' => 'required',
                    'password' => 'required'
                        )
        );
        
        if($this->validator->fails())
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public function getValidator() {

        return $this->validator;
    }    
    
    
    /*
    * Rehash password.
    */

    public function rehashPassword($password, $randomSalt) {
        
        $cost = 10;
        
        $passwordHash = $this->password->cryptBlowfish($password, $cost, $randomSalt);

        //Error with hashing.
        $this->testerBlowfishPw->testBlowfishPw($passwordHash);

        return $passwordHash;
    }
    
}
