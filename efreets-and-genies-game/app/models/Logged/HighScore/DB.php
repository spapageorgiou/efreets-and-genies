<?php

namespace Logged\HighScore;

/**
 * DB stuff for HighScore.
 */
class DB {
    /*
     * Show 10 highest scores.
     */

    public function showHighScores() {

        $query = "
         SELECT         
        `u`.username AS `username`,
        `hs`.name AS `name`,
        `hs`.score AS `score`,
        `rc`.race AS `race`,
        `hs`.updated_at AS `updated_at`
        FROM
        high_scores as `hs`
        INNER JOIN users AS `u` ON `hs`.user_id = `u`.id
        INNER JOIN races as `rc` ON `hs`.race_id = `rc`.id        
        ORDER BY `hs`.score
        DESC
        LIMIT 10
        ;
        ";

        try {
            $result = \DB::select($query);

            if(count($result) > 0){
                
                return $result;
            }
            else if(count($result) === 0){
                
                return false;
            }            
            
        } catch (Exception $ex) {
            \Log::error($ex->getMessage());
            \App::abort(500);
        }
    }

}
