<?php

namespace EfreetsAndGenies\HighScoreAJAX;

/*
 * DB code for HighScore AJAX.
 */

class DB {

    public function insertHighScore($queryParameters) {

        $query = "INSERT "
                . "INTO high_scores "
                . "(user_id, name, score, race_id, created_at) "
                . "VALUES "
                . "(?, ?, ?, ?, ?) "
                . "ON DUPLICATE KEY UPDATE "
                . "name = ?, "
                . "score = ?, "
                . "race_id = ? "
                . ";";

        try {

            $result = \DB::insert($query, $queryParameters); 

            return $result;
            
        } catch (Exception $ex) {
            
            \Log::error($ex->getMessage());
            \App::abort(500);
        }
    }

}
