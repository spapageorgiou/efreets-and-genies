<?php

/*
 * AJAX filters.
 */

Route::filter('generic_ajax', 'General\Laravel4X\Security\Filters\AJAX@genericFilter');

Route::filter('e_and_g_ajax', 'GeneralProject\Laravel4X\Security\Filters\AJAX@projectFilter');

Route::filter('user_id_check_ajax', 'GeneralProject\Laravel4X\Security\Filters\Authorization@checkUserId');

/*
 * Authorization filters.
 */

Route::filter('unlogged_pages', 'GeneralProject\Laravel4X\Security\Filters\Authorization@filterUnloggedPages');

Route::filter('logged_pages', 'GeneralProject\Laravel4X\Security\Filters\Authorization@filterLoggedPages');
