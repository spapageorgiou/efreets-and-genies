<?php

namespace GeneralProject\Laravel4X\Various\URLS;

/**
 * URL related stuff.
 */
class URL {
    /*
     * Holds URLs.
     */

    public function getLoggedURLs() {
        
        $urls = array();
        $urls['e-and-g'] = "Efreets And Genies";
        $urls['highscore'] = "High Score";
        $urls['help'] = "Help";
        $urls['logout'] = "Logout";

        return $urls;
    }

}
