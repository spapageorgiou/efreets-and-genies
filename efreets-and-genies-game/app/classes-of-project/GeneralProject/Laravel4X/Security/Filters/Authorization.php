<?php

namespace GeneralProject\Laravel4X\Security\Filters;

/*
 * Authorization.
 */

class Authorization {
    /*
     * Filter unlogged pages.
     */

    public function filterUnloggedPages() {
        // User is logged in.
        if (\Session::has('userId')) {
            //Redirect to game.
            return \Redirect::route('e-&-g');
        }
        //else access page.
    }

    /*
     * Filter logged pages.
     */

    public function filterLoggedPages() {
        //User is unlogged.
        if (!\Session::has('userId')) {
            //Redirect to login page.
            return \Redirect::route('home');
        }
        //else access page.
    }

    /*
     * Check that user id in page and in Session are the same.
     */
    public function checkUserId()
    {
        // If not equal, there is problem. Return Unauthorized.
        if(\Input::get('userId') != \Session::get('userId')){
        
            \Log::error("User id mismatch.");
            return \Response::make('Unauthorized', 401);        
            
        }
    }
}
