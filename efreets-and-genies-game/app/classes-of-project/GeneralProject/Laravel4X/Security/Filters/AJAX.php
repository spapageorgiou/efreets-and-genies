<?php

namespace GeneralProject\Laravel4X\Security\Filters;

/*
 * AJAX stuff for project.
 */

class AJAX {
    
    public function projectFilter()
    {
        //If save is set.
        if (!(\Input::has('userId') AND \Input::has('name') AND \Input::has('score') AND \Input::has('raceId'))) {
                   
            \Log::error("Input data for AJAX not found.");
            return \Response::make('Unauthorized', 401);        
            
        }
        
    }
}
