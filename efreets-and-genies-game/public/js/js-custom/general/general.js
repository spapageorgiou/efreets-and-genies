/**
 * Created by S.Papageorgiou on 8/23/14.
 */

/*
 * Utility methods.
 */

var general =
        {               
            debounceTimer: 200,          
            returnRandomIntFromRange: function (min, max)
            {
                return Math.floor(Math.random() * (max - min + 1) + min);
            },
            capitaliseFirstLetter: function (string)
            {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }
        };
