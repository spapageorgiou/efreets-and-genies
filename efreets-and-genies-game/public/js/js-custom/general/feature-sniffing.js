var JSON;

var features = {
    areFeaturesAvailable: function () {

        if (features.isThereJSON() && features.isThereCanvas()) {
            
            gameRunner.boot();
            
        }
        else {
            
            $("#no-features").removeClass("hide");
            
        }
    },
    isThereJSON: function () {

        if (JSON && typeof JSON.parse === 'function') {

            return true;

        }
        else {

            return false;
        }

    },
    isThereCanvas: function () {

        if (Modernizr.canvas) {

            return true;

        }
        else {

            return false;

        }

    }
};