/**
 * Created by S.Papageorgiou on 8/21/14.
 */

/*
 * Ajax related stuff.
 */
var database =
        {
            saveHighScore: function()
            {
                $.ajax(
                        {
                            async: false,
                            type: "POST",
                            dataType: 'json',
                            contentType: "application/json",
                            url: "ajax-pages/e-and-g-save-highscore",
                            data: JSON.stringify(
                                    {
                                        userId: user.id,
                                        raceId: character.details.race.details[character.details.race.chosen].raceId,
                                        name: character.details.name,
                                        score: character.details.score,
                                        _token: $("input[name=_token]").val()
                                    }),
                            success: function()
                            {
                                messages.appendText("High score has been saved.");
                            },
                            error: function()
                            {
                                messages.appendText("Failed to save high score.");
                            }

                        });

            }
        };

var stayAlive =
        {
            update: function()
            {
                $.ajax(
                        {
                            async: false,
                            type: "POST",
                            dataType: 'json',
                            contentType: "application/json",
                            url: "ajax-pages/e-and-g-update",
                            data: JSON.stringify(
                                    {                                   
                                        _token: $("input[name=_token]").val()
                                    }),
                            error: function()
                            {
                                messages.appendText("Error. Probably inactive for too long. You will not be able to save highscore.");
                            }

                        });

            }

        }        
        ;