/**
 * Created by S.Papageorgiou on 8/18/14.
 */

/*
 Object handles commands.
 */

var commands =
        {
            commands:
                    {
                        north: [["map", "movement"], "moveNorthChar", ["character"]],
                        south: [["map", "movement"], "moveSouthChar", ["character"]],
                        east: [["map", "movement"], "moveEastChar", ["character"]],
                        west: [["map", "movement"], "moveWestChar", ["character"]],
                        attack: [["character"], "attack", [""]],
                        race: [["character"], "showRace", [""]],
                        name: [["character"], "showName", [""]],
                        damage: [["character"], "showDamage", [""]],
                        health: [["character"], "showHealth", [""]],
                        level: [["character"], "showLevel", [""]],
                        skills: [["character"], "showSkills", [""]],
                        score: [["character"], "showScore", [""]],
                        "save high score": [["database"], "saveHighScore", [""]],
                        clear: [["messages"], "clear", [""]],
                        commands: [["commands"], "showCommands", [""]],
                        "new game": [["gameRunner"], "newGame", [""]]

                    },
            buildCommandsObjects: function (command)
            {
                /*
                 Build string with object, that can be object of object.
                 */

                var objects = "";

                for (var i = 0; i < this.commands[command][0].length; i++)
                {
                    objects += this.commands[command][0][i] + ".";
                }

                objects = objects.substring(0, objects.length - 1);

                return objects;
            },
            buildCommandsParameters: function (command)
            {

                /*
                 Build string with parameters (zero, one or more parameters exist in commands array).
                 */

                var parameters = "";

                for (var i = 0; i < this.commands[command][2].length; i++)
                {
                    parameters += this.commands[command][2][i] + ",";
                }

                parameters = parameters.substring(0, parameters.length - 1);

                return parameters;

            },
            evalCommand: function (commandString)
            {
                //Dynamically call method.
                try
                {                                                        
                        // Keep connection alive.
                        stayAlive.update();
                        //Dynamically call method.
                        eval(commandString);

                }
                catch (error)
                {
                    console.log(error);
                }


            },
            buildCommandButtonsAndAutocomplete: function ()
            {
                for (var command in this.commands)
                {
                    var objects = this.buildCommandsObjects(command);

                    var parameters = this.buildCommandsParameters(command);

                    var btn = "btn-command-";
                    //If command has space, turn it inot hyphen, so it can be put in the buttons id.
                    var commandId = command.replace(/ /g, "-");

                    var commandButton = $("<button class=\"btn btn-default\" id=\"" + btn + commandId + "\">" + command + "</button>");

                    $("#game-window-buttons").append(commandButton);

                    var commandString = objects + "." + commands.commands[command][1] + "(" + parameters + ")";

                    // Using a closure to create a new private scope
                    // fix: “close” the value of i inside createFunction, so it won't change
                    var closedValueOfCommand = function (commandString) {
                        return function () {
                            commands.evalCommand(commandString);
                        };
                    };

                    // Assign commands to buttons.        
                    $("#" + btn + commandId).bind("click",
                    _.debounce(closedValueOfCommand(commandString), general.debounceTimer));

                    // Add support for autocomplete commands to command input text field.
                    $('#autocomplete-commands').append("<option value='" + command + "'>");

                }
            },
            handle: function ()
            {
                /*
                 Easily call game commands.
                 */

                var command = $("#commands-field").prop('value');

                // Command exists, build string for eval and run it.
                if (this.commands.hasOwnProperty(command))
                {

                    var objects = this.buildCommandsObjects(command);

                    var parameters = this.buildCommandsParameters(command);

                    this.evalCommand(objects + "." + this.commands[command][1] + "(" + parameters + ")");

                }

                else
                {
                    messages.appendText("Command does not exist.");
                }
            },
            showCommands: function ()
            {
                var availableCommands = "Commands:\n";

                for (var key in this.commands)
                {
                    availableCommands += key + "\n";
                }

                //Remove last \n for nice text formatting.
                availableCommands = availableCommands.substring(0, availableCommands.length - 1);

                messages.appendText(availableCommands);
            }

        };
