/**
 * Created by S.Papageorgiou on 8/18/14.
 */


/*
 Map related staff.
 */
var map =
        {
            mapProperties:
                    {
                        roomsMarginX: 5,
                        roomsMarginY: 10,
                        roomsSpaceBetweenX: 30,
                        roomsSpaceBetweenY: 30,
                        mapY: 20,
                        roomWidth: 10,
                        roomHeight: 10
                    },
            maps:
                    {
                        deserts:
                                [
                                    [4, 4, 0, 0, 0, 0, 0],
                                    [4, 1, 0, 0, 0, 2, 0],
                                    [0, 0, 1, 0, 0, 0, 0],
                                    [3, 0, 0, 1, 1, 0, 0],
                                    [3, 3, 0, 0, 0, 1, 0],
                                    [3, 3, 3, 3, 0, 0, 1]
                                ]
                    },
            mapSquares:
                    {
                        0: {type: 'forest', fillStyle: 'green'},
                        1: {type: 'stream', fillStyle: 'cyan'},
                        2: {type: 'rock_formation', fillStyle: 'grey'},
                        3: {type: 'snow', fillStyle: 'white'},
                        4: {type: 'hills', fillStyle: '#A66829'}
                    },
            roomDescriptions:
                    {
                        0: "You are in a forest, full of trees and smaller plants. Birds are flying around and small animals and insects walk around making noises.",
                        1: "You are near a stream. Small animal life can be spotted in it and around it.",
                        2: "A rock formation is found here.",
                        3: "This is a patch of snow.",
                        4: "Small hills are found here."
                    },
            initialize: function ()
            {
                var canvas = document.getElementById("game-canvas");

                var context = canvas.getContext("2d");

                return context;
            },
            drawAtPosition: function (context, y, roomsMarginX, roomsSpaceBetweenX, mapY, roomsMarginY,
                    x, roomsSpaceBetweenY, roomWidth, roomHeight, text)
            {

                if (typeof text == 'undefined')
                {
                    return function ()
                    {
                        context.rect( (roomsMarginX + (x * roomsSpaceBetweenX)), (mapY + roomsMarginY + (y * roomsSpaceBetweenY)), roomWidth, roomHeight);
                    };
                }
                else
                {
                    return function ()
                    {
                        context.fillText(text, (roomsMarginX + (x * roomsSpaceBetweenX)), (mapY + roomsMarginY + (y * roomsSpaceBetweenY)), roomWidth);
                    };
                }

            },
            drawBackground: function ()
            {
                /*
                 Draw background.
                 */

                var context = this.initialize();

                var width = parseInt(document.getElementById("game-canvas").getAttribute("width"));

                var height = parseInt(document.getElementById("game-canvas").getAttribute("height"));

                context.rect(0, 0, width, height);

                context.fillStyle = "#C0C0C0";

                context.fill();

                context.lineWidth = 1;

                context.strokeStyle = 'navy';

                context.stroke();

                /*
                 Draw Map text.
                 */

                context.fillStyle = "blue";

                context.font = "bold 16px Arial";

                var mapY = this.mapProperties.mapY;

                context.fillText("Map", 90, mapY);

            },
            drawSquares: function ()
            {
                var context = this.initialize();

                /*
                 Draw rooms.
                 */

                var roomsMarginX = this.mapProperties.roomsMarginX;

                var roomsMarginY = this.mapProperties.roomsMarginY;

                var roomsSpaceBetweenX = this.mapProperties.roomsSpaceBetweenX;

                var roomsSpaceBetweenY = this.mapProperties.roomsSpaceBetweenY;

                var mapY = this.mapProperties.mapY;

                var roomWidth = this.mapProperties.roomWidth;

                var roomHeight = this.mapProperties.roomHeight;

                for (var y = 0; y < map["maps"]["deserts"].length; y++)
                {
                    var squares = map["maps"]["deserts"][y];

                    for (var x = 0; x < squares.length; x++)
                    {

                        context.beginPath();

                        this.drawAtPosition(context, y, roomsMarginX, roomsSpaceBetweenX, mapY, roomsMarginY,
                                x, roomsSpaceBetweenY, roomWidth, roomHeight)();

                        context.fillStyle = this.mapSquares[ this.maps.deserts[y][x] ].fillStyle;

                        context.fill();

                        context.strokeStyle = "black";

                        context.stroke();

                    }

                }


            },
            drawMap: function ()
            {

                this.drawBackground();

                this.drawSquares();

            },
            drawChar: function (character)
            {
                var context = this.initialize();

                var roomsMarginX = this.mapProperties.roomsMarginX;

                var roomsMarginY = this.mapProperties.roomsMarginY;

                var roomsSpaceBetweenX = this.mapProperties.roomsSpaceBetweenX;

                var roomsSpaceBetweenY = this.mapProperties.roomsSpaceBetweenY;

                var mapY = this.mapProperties.mapY;

                var roomWidth = this.mapProperties.roomWidth;

                var roomHeight = this.mapProperties.roomHeight;

                context.font = "15px Arial";

                context.fillStyle = "brown";

                this.drawAtPosition(context, character.details.positionY, roomsMarginX, roomsSpaceBetweenX, mapY, roomsMarginY,
                        character.details.positionX, roomsSpaceBetweenY, roomWidth, roomHeight, "@")();

                messages.appendText
                        (map.roomDescriptions[ map.maps.deserts[character.details.positionY][character.details.positionX] ]);

                opponent.appearOpponent();

            },
            movement:
                    {
                        moveChar: function (character, moveY, moveX)
                        {

                            try
                            {

                                if (typeof map.maps.deserts[character.details.positionY + moveY]
                                        [character.details.positionX + moveX] != 'undefined')
                                {
                                    character.details.positionY += moveY;
                                    character.details.positionX += moveX;

                                    //Refresh map.
                                    map.drawMap();

                                    map.drawChar(character);

                                }
                            }
                            catch (error)
                            {
                                console.log(error);
                            }
                        },
                        moveNorthChar: function (character)
                        {
                            map.movement.moveChar(character, -1, 0);
                        },
                        moveSouthChar: function (character)
                        {
                            map.movement.moveChar(character, +1, 0);
                        },
                        moveEastChar: function (character)
                        {
                            map.movement.moveChar(character, 0, +1);
                        },
                        moveWestChar: function (character)
                        {
                            map.movement.moveChar(character, 0, -1);
                        }
                    }
        };

