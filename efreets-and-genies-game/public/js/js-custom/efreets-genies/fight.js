/* 
 Created by Solon Papageorgiou on 09/13/2014.
 */

/*
 * Fight related code.
 */
var fight = {
    checkAttack: function ()
    {
        var opponentLevel = character.findWhoIs("opponent");

        if (opponent.spotOpponent(opponentLevel))
        {
            fight.attackHim(opponentLevel);

            character.haveYouWon();
        }
        else
        {
            messages.appendTextNoNewLine("There is nobody to attack here.");
        }
    },
    attackHim: function (opponentLevel)
    {
        var opponentName = opponent.details.race.details[opponentLevel].name;

        messages.appendTextNoNewLine("You attacked " + opponentName + ".");

        var raceId = character.details.race.details[character.details.race.chosen].raceId;

        var skills = character.skills;

        var race = character.details.race.details[character.details.race.chosen].name;

        // Fight begins.           
        fight.skillsLoopFight(skills, race, opponentLevel, fight.fightCallback);

        //Fight ends.
        fight.doEndOfFightStuff(raceId, opponentLevel, opponentName);

    },
    doEndOfFightStuff: function (raceId, opponentLevel, opponentName)
    {
        if (character.details.race.details[raceId].health < 1)
        {
            messages.appendTextNoNewLine("You escape combat.");

            // Penalize character for low health.
            character.penalizeScore();
        }

        if (opponent.details.race.details[opponentLevel].health < 1)
        {
            messages.appendTextNoNewLine(opponentName + " died.");

            // Character killed an opponent. Increase score.                    
            character.details.score += 5;

            // Repopulate opponent.
            opponent.details.race.details[opponentLevel].health =
                    opponent.details.race.details[opponentLevel].maxHealth;

        }

        opponent.appearOpponent();

        // Character replenishes health after fight.
        character.replenishHealth(raceId);

        // Append a line to improve readability. 
        messages.appendTextNoNewLine("");

    },
    skillsLoopFight: function (skills, race, opponentLevel, callback)
    {
        var pickedSkill = general.returnRandomIntFromRange(character.minimumSkill, character.maximumSkill);

        var currentSkill = 0;

        for (var appliedTo in skills[race])
        {
            for (var effect in skills[race][appliedTo])
            {
                for (var skillType in skills[race][appliedTo][effect])
                {
                    for (var skill in skills[race][appliedTo][effect][skillType])
                    {
                        currentSkill++;                        

                        if (pickedSkill === currentSkill)
                        {
                            callback(appliedTo, skillType, race, effect, skill, skills, opponentLevel);
                        }
                        else
                        {
                            continue;
                        }
                    }
                }
            }
        }
    },
    fightCallback: function (appliedTo, skillType, race, effect, skill, skills, opponentLevel)
    {
        var raceId = character.details.race.details[character.details.race.chosen].raceId;

        var whoIs = character.findWhoIs(appliedTo);

        var tempOriginal = fight.applySkill(appliedTo, skillType, race, effect, skill, skills, whoIs);

        fight.fight(opponentLevel, raceId);

        fight.reverseEffect(effect, tempOriginal, appliedTo, skillType, whoIs);

        // After all the fight occured, try to train skill.
        character.trainSkill(appliedTo, skillType, race, effect, skill, skills);

    },
    applySkill: function (appliedTo, skillType, race, effect, skill, skills, whoIs)
    {
        var applySkill =
                window[appliedTo].details.race.details[whoIs][skillType];

        var temp = applySkill;

        var percentageToHappen = 50 / 100;

        if (Math.random() <= percentageToHappen) {

            var skillValue = skills[race][appliedTo][effect][skillType][skill]["formula"] *
                    skills[race][appliedTo][effect][skillType][skill]["level"];

            applySkill += skillValue;

            var message = skills[race][appliedTo][effect][skillType][skill]["message"];

            window[appliedTo].details.race.details[whoIs][skillType] = applySkill;

            messages.appendTextNoNewLine("Your " + skill + message + applySkill + " " + skillType + ".");

        }
        else
        {
            messages.appendTextNoNewLine("You failed to use " + skill + ".");
        }

        return temp;
    },
    fight: function (opponentLevel, characterRaceId)
    {
        var isOpponent = opponentLevel;

        var isCharacter = characterRaceId;

        var percentageToHappen = 50 / 100;

        if (Math.random() <= percentageToHappen)
        {
            //Character attack.
            opponent.details.race.details[isOpponent].health -= character.details.race.details[isCharacter].damage;

            messages.appendTextNoNewLine("You damaged opponent.");
        }
        else
        {
            messages.appendTextNoNewLine("You failed to damage opponent.");
        }

        fight.opponentHealth(isOpponent);

        if (Math.random() <= percentageToHappen)
        {
            //Opponent attack.
            character.details.race.details[isCharacter].health -= opponent.details.race.details[isOpponent].damage;

            messages.appendTextNoNewLine("Opponent damaged you.");
        }
        else
        {
            messages.appendTextNoNewLine("Opponent failed to damage you.");
        }

        fight.characterHealth(isCharacter);
    },
    opponentHealth: function (isOpponent)
    {
        var opponentHealth = opponent.details.race.details[isOpponent].health;

        if (opponentHealth < 1) {
            messages.appendTextNoNewLine("Opponent's health is low.");
        }
        else
        {
            messages.appendTextNoNewLine("Opponent's health is: " + opponentHealth + ".");
        }
    },
    characterHealth: function (isCharacter)
    {
        var characterHealth = character.details.race.details[isCharacter].health;

        if (characterHealth < 1)
        {
            messages.appendTextNoNewLine("Your health is low.");
        }
        else
        {
            messages.appendTextNoNewLine("Your health is: " + characterHealth + ".");
        }

    },
    reverseEffect: function (effect, temp, appliedTo, skillType, whoIs)
    {
        // If effect is temporary, reverse attributes.
        if (effect === "temporaryEffect")
        {
            window[appliedTo].details.race.details[whoIs][skillType] = temp;
        }
    }
};

