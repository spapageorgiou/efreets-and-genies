/**
 * Created by S.Papageorgiou on 8/19/14.
 */

/*
 * Messages textarea related code.
 */

var messages =
        {   
            lineToTruncate: 1000,
            rowToTruncate: 300,
            message: null,
            appendTextNoNewLine: function(message)
            {
                messages.appendMain(message, "");
                
                messages.scrollTextArea();

            },
            appendText: function(message)
            {
                messages.appendMain(message, "\n");
                
                messages.scrollTextArea();

            },
            appendMain: function(message, _newLineEnd)
            {               
                var newLineEnd = _newLineEnd || "";
                
                var originalMsg = messages.truncateLongMessage(messages.message.val());
                
                messages.message.val(originalMsg + "\n" + message + newLineEnd);
                
            },
            truncateLongMessage: function(message)
            {
                /* We need to truncate a long message, because a large message,
                 * will slow down the browser.
                 */
                
                // 1000 lines of games's text area are about 30000 bytes.
                
                var rowsToTruncate = messages.rowToTruncate;
                
                var lineToTruncate = messages.lineToTruncate;      
                
                var rows = message.split("\n").length; 
                
                if(rows >= lineToTruncate)
                {
                    // break the textblock into an array of lines
                    var lines = message.split('\n');
                    // remove one line, starting at the first position
                    lines.splice(0, rowsToTruncate);
                    // join the array back into a single string
                    var message = lines.join('\n');
                }                
                
                return message;
                
            },
            scrollTextArea: function()
            {
                messages.message.scrollTop(messages.message[0].scrollHeight);
            },
            clear: function()
            {
                messages.message.val("");
            }

        };

