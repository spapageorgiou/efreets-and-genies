/* 
 * Created by S.Papageorgiou on 09/09/14.
 */

/*
 * Opponent.
 */
var opponent =
        {
            details:
                    {
                        race:
                                {                                    
                                    details:
                                            {
                                                1:
                                                        {
                                                            name: "Golem",
                                                            raceId: 1,
                                                            damage: 80,
                                                            health: 80,
                                                            maxHealth: 80,
                                                            location: [
                                                            [0, 1, 0, 0, 0, 0, 0],
                                                            [1, 0, 0, 0, 0, 0, 0],
                                                            [0, 0, 0, 0, 0, 0, 0],
                                                            [0, 0, 0, 0, 0, 0, 0],
                                                            [0, 0, 0, 0, 0, 0, 0],
                                                            [0, 0, 0, 0, 0, 0, 0]
                                                            ]
                                                        },
                                                2:
                                                        {
                                                            name: "Lich",
                                                            raceId: 2,
                                                            damage: 90,
                                                            health: 100,
                                                            maxHealth: 100,
                                                            location: [
                                                            [0, 0, 0, 0, 0, 0, 0],
                                                            [0, 0, 0, 0, 0, 1, 0],
                                                            [0, 0, 0, 0, 0, 0, 0],
                                                            [0, 0, 0, 0, 0, 0, 0],
                                                            [0, 0, 0, 0, 0, 0, 0],
                                                            [0, 0, 0, 0, 0, 0, 0]
                                                            ]
                                                        },
                                                3:
                                                        {
                                                            name: "Behemoth",
                                                            raceId: 2,
                                                            damage: 100,
                                                            health: 120,
                                                            maxHealth: 120,
                                                            location: [
                                                            [0, 0, 0, 0, 0, 0, 0],
                                                            [0, 0, 0, 0, 0, 0, 0],
                                                            [0, 0, 0, 0, 0, 0, 0],
                                                            [0, 0, 0, 0, 0, 0, 0],
                                                            [0, 1, 0, 0, 0, 0, 0],
                                                            [1, 0, 0, 0, 0, 0, 0]
                                                            ]
                                                        }
                                            }

                                }
                    },
                    spotOpponent: function(whoIs)
                    {
                        // Find if there is a monster where character is.
                        if(opponent.details.race.details[whoIs].location[character.details.positionY]
                        [character.details.positionX] === 1)
                        {
                            return true;
                        }               
                        else
                        {
                            return false;
                        }
                    },
                    appearOpponent: function()
                    {                               
                        var opponentLevel = character.findWhoIs("opponent");
                        
                        if(opponent.spotOpponent(opponentLevel))
                        {
                            messages.appendText("There is a " + opponent.details.race.details[opponentLevel].name + " here.");
                        }
                    }
        };