/**
 * Created by S.Papageorgiou on 8/18/14.
 */

/*
 * Character related stuff.
 */
var character =
        {
            details:
                    {
                        positionX: 0,
                        positionY: 0,
                        wonGame: false,
                        race:
                                {
                                    chosen: null,
                                    details:
                                            {
                                                1:
                                                        {
                                                            name: "Efreet",
                                                            raceId: 1,
                                                            damage: 30,
                                                            health: 150,
                                                            maxHealth: 150
                                                        },
                                                2:
                                                        {
                                                            name: "Genie",
                                                            raceId: 2,
                                                            damage: 20,
                                                            health: 200,
                                                            maxHealth: 200
                                                        }
                                            }

                                },
                        name: null,
                        score: 0
                    },
            skills:
                    {
                        Efreet:
                                {
                                    opponent:
                                            {
                                                permanentEffect:
                                                        {
                                                            health:
                                                                    {
                                                                        curse:
                                                                                {
                                                                                    formula: -1,
                                                                                    level: 1,
                                                                                    message: " reduces opponent's health to "
                                                                                }
                                                                    }
                                                        }
                                            },
                                    character:
                                            {
                                                temporaryEffect:
                                                        {
                                                            damage:
                                                                    {
                                                                        "fiering aura":
                                                                                {
                                                                                    formula: +1,
                                                                                    level: 1,
                                                                                    message: " is about to be unleashed and damage opponent with "
                                                                                },
                                                                        "fiering fists":
                                                                                {
                                                                                    formula: +2,
                                                                                    level: 1,
                                                                                    message: " are ready, in position to damage opponent with "
                                                                                }
                                                                    }
                                                        }
                                            }
                                },
                        Genie:
                                {
                                    opponent:
                                            {
                                                temporaryEffect:
                                                        {
                                                            damage:
                                                                    {
                                                                        "protective aura":
                                                                                {
                                                                                    formula: -1,
                                                                                    level: 1,
                                                                                    message: " reduces opponent's damage to "
                                                                                }
                                                                    }
                                                        }
                                            },
                                    character:
                                            {
                                                temporaryEffect:
                                                        {
                                                            damage:
                                                                    {
                                                                        "mental blast":
                                                                                {
                                                                                    formula: "+1.5",
                                                                                    level: 1,
                                                                                    message: " is about to be unleashed and damage opponent with "
                                                                                }
                                                                    }
                                                        },
                                                permanentEffect:
                                                        {
                                                            health:
                                                                    {
                                                                        "heal":
                                                                                {
                                                                                    formula: +2,
                                                                                    level: 1,
                                                                                    message: " increases your health to "
                                                                                }
                                                                    }
                                                        }
                                            }
                                }
                    },
            level: {1: [0, 19], 2: [20, 39], 3: [40, 60]},            
            skillLevel: {1: [0, 6], 2: [7, 13], 3: [14, 20]},            
            levelsMap: {1: "amateur", 2: "average", 3: "excellent"},
            minimumSkill: 1,
            maximumSkill: 3,
            giftHealth: 60,
            healthDenominatorMin: 5,
            healthDenominatorMax: 10,
            chooseEfreet: function()
            {
                character.details.race.chosen = 1;
            },
            chooseGenie: function()
            {
                character.details.race.chosen = 2;
            },
            buildName: function()
            {
                var vowels = ["a", "e", "i", "o", "u"];

                var vowelBegini = 0;

                var vowelEndi = 4;

                var consonants = ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n",
                    "p", "q", "r", "s", "t", "v", "w", "x", "y", "z"];

                var consBegini = 0;

                var consEndi = 20;

                var name = "";

                //Loop 4 times, to create a name. 4 times means 4 syllabes for a name, which is enough.        
                for (var i = 0; i < 4; i++)
                {
                    name = name + vowels[general.returnRandomIntFromRange(vowelBegini, vowelEndi)];

                    name = name + consonants[general.returnRandomIntFromRange(consBegini, consEndi)];
                }

                name = general.capitaliseFirstLetter(name);

                character.details.name = name;

            },
            haveYouWon: function()
            {
                var winScore = 100;
                
                // If character has high enough score, he/she wins the game.
                if(character.details.wonGame === false)
                {
                    if(character.details.score >= winScore)
                    {
                        // Character can win only once.
                        character.details.wonGame = true;
                        messages.appendText("You have won the game! You can still continue playing.");
                    }
                    
                }
                
            },
            penalizeScore: function()
            {              
              var penalty = 1;
                
              //As a penalty for having such low health (sort of death), reduce score.
              character.details.score -= penalty;                
            },
            replenishHealth: function(raceId)
            {
                var giftHealth = character.giftHealth;
                // Give some health to charactet, because with a vast negative 
                // health, the character will have trouble recovering and 
                // winning fights.                
                if(character.details.race.details[raceId].health < 1)
                {
                   character.details.race.details[raceId].health = giftHealth; 
                   
                   messages.appendTextNoNewLine("You were very exhausted. It felt as if you were to die. " +
                           "Something supernatural happened and you feel much better now.");
                }
                
                var denominatorMin = character.healthDenominatorMin;
                
                var denominatorMax = character.healthDenominatorMax;
                
                var denominator = general.returnRandomIntFromRange(denominatorMin, denominatorMax);
                
                var numerator = 1;
                
                var divisionQuotient = numerator/denominator;
                
                var characterMaxHealth = character.details.race.details[raceId].maxHealth;
                
                var healthBonus = Math.floor(divisionQuotient * characterMaxHealth); 
                
                character.details.race.details[raceId].health += healthBonus;
                
                messages.appendTextNoNewLine("You rest and replenish health. " + 
                "Your health improved by: " + healthBonus + ".");
                
                if(character.details.race.details[raceId].health >= characterMaxHealth)
                {
                   character.details.race.details[raceId].health = characterMaxHealth;
                   
                   messages.appendTextNoNewLine("You fully replenished your health.");
                }

                  
            },
            trainSkill: function(appliedTo, skillType, race, effect, skill, skills)
            {
              //Character level should not exceed 60, so no skill should exceed 20.
              var maxSkill = 20;    
              
              var skillIncrement = 1;
              
              var percentageToHappen = 30/100;
              
              if((skills[race][appliedTo][effect][skillType][skill]["level"] + 1) <= maxSkill)
              {
                  // Skill increases some times.
                  if(Math.random() <= percentageToHappen)
                  {
                      character.skills[race][appliedTo][effect][skillType][skill]["level"] += skillIncrement;
                      
                      messages.appendText("You feel more skilled in " + skill + ".");
                  }
              }
            },
            calculateCharacterSkillPoints: function(race, skills)
            {
                var skillPoints = 0;

                for (var appliedTo in skills[race])
                {
                    for (var effect in skills[race][appliedTo])
                    {
                        for (var skillType in skills[race][appliedTo][effect])
                        {
                            for (var skill in skills[race][appliedTo][effect][skillType])
                            {
                                skillPoints += skills[race][appliedTo][effect][skillType][skill]["level"];
                            }
                        }
                    }
                }

                return skillPoints;

            },
            getLevel: function(skillPoints, typeOfLevel) {                
                var calculatedLevel = 0;
                for (var level in character[typeOfLevel])
                {
                    if (skillPoints >= character[typeOfLevel][level][0] && skillPoints <= character[typeOfLevel][level][1])
                    {
                        calculatedLevel = level;
                        return calculatedLevel;
                    }
                }
            },
            findWhoIs: function(appliedTo)
            {
                var race = character.details.race.details[character.details.race.chosen].name;

                if (appliedTo === "character")
                {
                    return character.details.race.details[character.details.race.chosen].raceId;
                }
                else if (appliedTo === "opponent")
                {
                    var skillPoints = character.calculateCharacterSkillPoints(race, character.skills);

                    return character.getLevel(skillPoints, "level");
                }
            },
            skillsLoop: function(skills, race, callback)
            {
                for (var appliedTo in skills[race])
                {
                    for (var effect in skills[race][appliedTo])
                    {
                        for (var skillType in skills[race][appliedTo][effect])
                        {
                            for (var skill in skills[race][appliedTo][effect][skillType])
                            {

                                callback(appliedTo, skillType, race, effect, skill, skills);

                            }
                        }
                    }
                }
            },
            showSkillsCallback: function(appliedTo, skillType, race, effect, skill, skills)
            {
              var skillPoints = skills[race][appliedTo][effect][skillType][skill]["level"];  
              
              var level = character.getLevel(skillPoints, "skillLevel");

              var mappedLevel = character.levelsMap[level];
              
              messages.appendText(skill + ": " + mappedLevel + ".");
            },
            resetSkills: function(race)
            {
                var skills = character.skills;
                
                for (var appliedTo in skills[race])
                {
                    for (var effect in skills[race][appliedTo])
                    {
                        for (var skillType in skills[race][appliedTo][effect])
                        {
                            for (var skill in skills[race][appliedTo][effect][skillType])
                            {
                                
                                character.skills[race][appliedTo][effect][skillType][skill]["level"] = 1;

                            }
                        }
                    }
                }
            },                    
            attack: function()
            { 
              fight.checkAttack();
            },
            newGame: function()
            {
                var raceId = character.details.race.chosen;
                
                var race = character.details.race.details[raceId].name;
                
                character.resetSkills(race);
                
                character.details.positionX = 0;

                character.details.positionY = 0;
                
                character.details.wonGame = false;

                character.details.race.chosen = null;

                character.details.name = null;

                character.details.score = 0;
                
                character.details.race.details[raceId].health = character.details.race.details[raceId].maxHealth;
            },
            showRace: function()
            {
                messages.appendText("Your race is: " + character.details.race.details[character.details.race.chosen].name + ".");
            },
            showName: function()
            {
                messages.appendText("Your name is: " + character.details.name + ".");
            },
            showDamage: function()
            {
                messages.appendText("Your damage is: " + character.details.race.details[character.details.race.chosen].damage);
            },
            showHealth: function()
            {
                messages.appendText("Your health is: " + character.details.race.details[character.details.race.chosen].health);
            },
            showScore: function()
            {
                messages.appendText("Your score is: " + character.details.score);
            },
            showLevel: function()
            {
                var race = character.details.race.details[character.details.race.chosen].name;

                var skillPoints = character.calculateCharacterSkillPoints(race, character.skills);

                var level = character.getLevel(skillPoints, "level");

                var mappedLevel = character.levelsMap[level];

                messages.appendText("Your level is: " + mappedLevel + ".");
            },
            showSkills: function()
            {
                messages.appendText("Skills: ");
                
                var skills = character.skills;
                
                var race = character.details.race.details[character.details.race.chosen].name;
                
                character.skillsLoop(skills, race, character.showSkillsCallback);
            }

        };
