/**
 * Created by S.Papageorgiou on 8/18/14.
 */

/*
 * Concerned with running the game.
 */

var gameRunner =
        {
            create: function ()
            {
                $("#game-intro-message").removeClass("hide");

                $("#start-game").bind("click", function ()
                {
                    $("#game-intro-message").addClass("hide");

                    $("#race-message").removeClass("hide");

                    $("#choose-race").removeClass("hide");

                });
            },
            setup: function ()
            {
                $("#race-message").addClass("hide");

                $("#choose-race").addClass("hide");

                $("#messages").val("");

                commands.buildCommandButtonsAndAutocomplete();

                character.buildName();

                messages.message = $('#messages');

                character.showRace();

                character.showName();

                map.drawMap();

                map.drawChar(character);

                $("#main-window").removeClass("hide");

                $("#game-panel-command-buttons").removeClass("hide");

                $("#game-panel-messages").removeClass("hide");

            },
            boot: function ()
            {
                $("#choose-efreet").bind("click", character.chooseEfreet).bind('click', gameRunner.setup);

                $("#choose-genie").bind("click", character.chooseGenie).bind('click', gameRunner.setup);

                $("#submit-command").bind("click", _.debounce(function ()
                {
                    commands.handle();
                    //Move focus on commands field.
                    $("#commands-field").focus();
                }, general.debounceTimer));

                $("#commands-field").keyup(_.debounce(function (e) {
                    // On press enter submit command button is pressed.
                    if (e.keyCode === 13)
                    {
                        commands.handle();
                    }
                }, general.debounceTimer));

                gameRunner.create();
            },
            newGame: function ()
            {
                character.newGame();

                $("#game-intro-message").removeClass("hide");
                $("#main-window").addClass("hide");
                //Remove buttons, so that they can repopulated by setup method.
                $("#game-window-buttons").empty();
                $("#game-panel-command-buttons").addClass("hide");
                $("#game-panel-messages").addClass("hide");
                gameRunner.create();
            }

        }

// Document is ready, run JavaScript.
$(document).ready(function () {

    features.areFeaturesAvailable();

});