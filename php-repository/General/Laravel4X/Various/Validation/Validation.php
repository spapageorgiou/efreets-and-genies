<?php

namespace General\Laravel4X\Various\Validation;

/*
 * Validation stuff.
 */

class Validation {
    /*
     * Not exists in database.
     */

    public function notExists() {

        \Validator::extend('not_exists', function($attribute, $value, $parameters) {

            $parameter = array();

            $parameter[] = $value;

            $query = "SELECT "
                    . "COUNT(" . $parameters[1] . ") AS `count` "
                    . "FROM " . $parameters[0] . " "
                    . "WHERE "
                    . $parameters[1] . " = ? "
                    . ";";

            try {
                $results = \DB::SELECT($query, $parameter);

                if (count($results) === 1) {

                    $resultCount = (int)$results[0]->count;                   
                    
                    if ($resultCount === 0) {

                        return true;
                    } else if ($resultCount === 1) {

                        return false;
                    }
                }
            } catch (Exception $ex) {

                \Log::error($ex->getMessage());
                \App::abort(500);
            }
        });
    }

    /*
     * Captcha code. The following two functions form the Captcha.
     */

    public function makeCaptcha($sessionName) {

        //If Session has been set, reuse existing captcha string.
        if(!\Session::has($sessionName)){
            
            $stringCaptcha = \Str::random(6, 'alpha');

            // Save Captcha text for later comparison.
            \Session::put($sessionName, $stringCaptcha);
        }
        else{
            
            $stringCaptcha = \Session::get($sessionName);            
        }

        $width = 100;
        $height = 25;
        $image = imagecreatetruecolor($width, $height);

        $textColor = imagecolorallocate($image, 130, 130, 130);

        $bgColor = imagecolorallocate($image, 190, 190, 190);

        imagefilledrectangle($image, 0, 0, $width, $height, $bgColor);

        imagestring($image, 5, 16, 4, $stringCaptcha, $textColor);

        // Capture image. Don't save it. This way you don't need to make captcha images unique per visitor,
        // to avoid browsers overwriting captchas and displaying wrong cpatchas to visitors.
        ob_start();
        imagejpeg($image);
        $jpg = ob_get_clean();

        return "data:image/jpeg;base64," .
                base64_encode($jpg);
    }

    public function validateCaptcha(){
        
        \Validator::extend('validate_captcha', function($attribute, $value, $parameters) {
            
            if(\Session::get($parameters[0]) === \Input::get($parameters[0])) {
                return true;
            } else {
                return false;
            }
            
        });
    }
    
}
