<?php

namespace General\Laravel4X\Security\Validation;

/*
 * Validation related stuff.
 */

class Validation {
    /*
     * Check if Blowfish password hash has problems.
     * If method does not abort, you can then do what you need
     * with the password hash elsewhere in code. 
     */

    public function testBlowfishPw($passwordHash) {

        if ($passwordHash === false) {
            
            \Log::error('Failed. Password problem. Blowfish not found.');
            
            \App::abort(500);

            
        } else if (strlen($passwordHash) < 13) {
            
            \Log::error('Failed. Password problem. Hashing issue.');
            
            \App::abort(500);

            
        }
    }

}
