<?php

namespace General\Laravel4X\Security\Filters;

/*
 * AJAX related code.
 */
class AJAX {

    /*
     * Laravel filter.
     */
    public function genericFilter() {

        if (!(\Request::ajax() AND \Request::isJson())) {

            \Log::error("Request is not AJAX nor JSON.");
            
            return \Response::make('Unauthorized', 401);
        }
    }

}
