<?php

namespace General\Laravel4X\Security\Filters;

/*
 * XSS related code.
 */
class XSS {
    /*
     * Laravel filter.
     */

    public function genericFilter() {
        
        $arrayHelper = new \General\Utilities\Fundamentals\Helpers\ArrayHelper;
        
        // Strip tags from user input to guard against XSS attacks.
        \Input::merge($arrayHelper->arrayStripTags(\Input::all()));
    }



}
