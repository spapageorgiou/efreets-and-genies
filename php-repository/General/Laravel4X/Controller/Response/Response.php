<?php

namespace General\Laravel4X\Controller\Response;

/*
 * Responses.
 */
class Response {
    
    /*
     * JSON response.
     */
    
    public function returnJSON()
    {
        $headers = array("application/json");

        $response = \Response::make("{}", 200);

        $response->header('Content-Type', $headers);
        
        return $headers;
    }
}
