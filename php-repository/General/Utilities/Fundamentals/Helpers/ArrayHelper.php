<?php

namespace General\Utilities\Fundamentals\Helpers;

/*
 * Array helper functions.
 *
 */
class ArrayHelper {

    /*
    * Strips tags from array.
    */
    public function arrayStripTags($array) {
        $result = array();
        foreach ($array as $key => $value) {
            $key = strip_tags($key);
            if (is_array($value)) {
                $result[$key] = $this->arrayStripTags($value);
            } else {
                $result[$key] = strip_tags($value);
            }
        }
        return $result;
    }
}
