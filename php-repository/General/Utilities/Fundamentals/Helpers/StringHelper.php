<?php

namespace General\Utilities\Fundamentals\Helpers;

/**
 * StringHelper has helper functions for strings.
 *
 */
class StringHelper {
    /*
     * Get a raddom alphanumeric string. Good for e.g. creating Blowfish salts which
     * uses 0-9a-zA-Z.
     */

    public function getRandomAlphaNum($length) {

        $character_array = array_merge(range('a', 'z'), range('A', 'Z'), range(0, 9));

        $string = "";
        for ($i = 0; $i < $length; $i++) {
            $string .= $character_array[rand(0, (count($character_array) - 1))];
        }
        return $string;
    }

}
