<?php

namespace General\Utilities\Fundamentals\Security;

/*
 * Password related stuff.
 */
class Password {

    /*
     * Encrypt password with Blowfish.
     * //Cost should be two digits.
     * Salt should be 22 chars long.
     */
    public function cryptBlowfish($password, $cost, $salt){
        
        $saltStart = "$2a$";
                        
        $cost = $cost . "$";
        
        $saltEnd = "$";
        
        //If algorithm exists.
        if (CRYPT_BLOWFISH == 1) {
            return crypt($password, $saltStart . $cost . $salt . $saltEnd);
        }
        else{            
            return false;
        }

    }
}
