#Efreets And Genies

Efreets And Genies is an old school, mud like small game. In Laravel 4.2, jQuery, MySQL and Bootstrap 3.0.

#Summary

In Efreets And Genies, you choose an Efreet or a Genie as race and move around killing monsters and increasing your skills. Type commands + enter in the text field or click "Commands" button, to see the list of your available commands. There are 3 types of monsters that appear according to your level. You have to find them on the map. The highest you can get in skills and level is excellent. You don't die in this game, but your score is penalized if your health is low. If you reach score of 100, you win the game, but you can continue playing.

#Requirements

PHP >= 5.4

MCrypt PHP Extension

As of PHP 5.5, some OS distributions may require you to manually install the PHP JSON extension. 

MySQL 5.5.32

Apache 2.4.4

Install database from efreets-and-genies-game\app\my-schema.
To run application locally, add mysql credentials in efreets-and-genies-game\app\config\local\database.php.
To run it in production, add mysql credentials in efreets-and-genies-game\app\config\database.php.

#Use

Run Web application from your browser.
